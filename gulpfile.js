var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');

gulp.task('sass', function(){
	return gulp.src('app/sass/main.sass')
		.pipe(sass())
		.pipe(gulp.dest('app/css'))
});
gulp.task('watch', function(){
	gulp.watch('app/sass/**/*.sass', ['sass']);
});

gulp.task('browser-sync', function() {
    browserSync({
       server: {
           baseDir: 'app'
       }
    });
});

